package AngajatiApp.model;

import AngajatiApp.model.exceptions.InvalidEmployeeException;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {
    Employee employee = new Employee("Geza","Deak","1950518313707", DidacticFunction.ASISTENT,10.10);




    @Test
    void constr(){
        Employee employee2 = new Employee("Geza","Deak","1950518313707", DidacticFunction.ASISTENT,10.10);
        assertNotNull(employee2);

    }

    @Test
    @Order(1)
    void getFirstName() {
        assertEquals("Geza",employee.getFirstName());
    }

    @Test
    @Order(2)
    void setFirstName() {
        employee.setFirstName("Alex");
        assertEquals("Alex",employee.getFirstName());
    }

    @Test
    @Order(3)
    void getLastName() {
        assertEquals("Deak",employee.getLastName());
    }

    @Test
    @Order(4)
    void setLastName() {
        employee.setLastName("Pop");
        assertEquals("Pop",employee.getLastName());
    }

    @Test
    @Order(5)
    void getCnp() {
        assertEquals("1950518313707",employee.getCnp());
    }

    @Test
    @Order(6)
    void setCnp() {
        employee.setCnp("2950118313707");
        assertTimeout(Duration.ofSeconds(2),()->{
            assertEquals("2950118313707",employee.getCnp());
        });


    }


    @ParameterizedTest
    @ValueSource(doubles = {12.1,45.5,20,1})
    @Order(9)
    void  setSalary(double salary){
        System.out.println(salary);
        employee.setSalary(salary);
        assertEquals(salary,employee.getSalary());
    }

    @Test
    @Order(8)
    void getSalary(){
        assertEquals(10.10, employee.getSalary());
    }


    @Test
    @Order(7)
    @Timeout(value = 500, unit = TimeUnit.MILLISECONDS)

    void getEmployeeFromString_should_throwEmployeeExceptionIfStringInvalid(){
        assertThrows(InvalidEmployeeException.class,()->{
                      Employee.getEmployeeFromString("abcs",3);
        });

    }


}