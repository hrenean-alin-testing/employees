package AngajatiApp.controller;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import AngajatiApp.model.DidacticFunction;
import AngajatiApp.model.Employee;
import AngajatiApp.repository.EmployeeRepositoryInterface;
import AngajatiApp.repository.InMemoryEmployeeRepository;
import AngajatiApp.view.EmployeeView;

public class EmployeeController {
	
	private EmployeeRepositoryInterface employeeRepository;
	private EmployeeView employeeView;

	private static Scanner scanner;
	public static void runMenu() throws IOException {

		EmployeeRepositoryInterface employeesRepository = new InMemoryEmployeeRepository();
		EmployeeController employeeController = new EmployeeController(employeesRepository);
		scanner = new Scanner(System.in);
		while (true) {
			employeeController.printMenu();
			int command;
			try {
				command = scanner.nextInt();
			} catch(Exception e) {
				System.out.println("Exit!");
				return;
			}
			switch (command) {
				case 1:
					Employee employee = getEmployeeFromInput();
					employeeController.addEmployee(employee);
					System.out.println("Employee was added");
					break;
				case 2:
					System.out.println("Dati id-ul angajatului: ");
					int idOldEmployee = scanner.nextInt();
					Employee oldEmployee = employeeController.findEmployeeById(idOldEmployee);
					System.out.println("Dati noua functie didactica: ");
					String newFunction = scanner.next();
					employeeController.modifyEmployee(oldEmployee, getDidacticFunction(newFunction));
					break;
				case 3:
					for(Employee employeeItem : employeeController.getSortedEmployeeList())
					{
						System.out.println(employeeItem.toString());
					}
					break;
				default:
					System.out.println("Exit!");
					return;
			}
		}
	}
	
	public EmployeeController(EmployeeRepositoryInterface employeeRepository) {
		this.employeeRepository = employeeRepository;
		this.employeeView = new EmployeeView();
	}
	
	public void addEmployee(Employee employee) throws IOException {
		employeeRepository.addEmployee(employee);
	}
	
	public List<Employee> getEmployeesList() {
		return employeeRepository.getEmployeeList();
	}
	
	public void modifyEmployee(Employee oldEmployee, DidacticFunction newFunction) {
		employeeRepository.modifyEmployeeFunction(oldEmployee, newFunction);
	}
	
	public List<Employee> getSortedEmployeeList() {
		return employeeRepository.getEmployeeByCriteria();
	}
	
	public void printMenu() {
		employeeView.printMenu();
	}

	public Employee findEmployeeById(int idOldEmployee) {
		return employeeRepository.findEmployeeById(idOldEmployee);
	}

	//modificat
	private static Employee getEmployeeFromInput() {
		try{
			scanner.nextLine();
			System.out.println("First name: ");
			String firstName = scanner.nextLine();
			System.out.println("Last name: ");
			String lastName = scanner.nextLine();
			System.out.println("CNP: ");
			String cnp = scanner.nextLine();
			System.out.println("Functie didactica: ");
			String didacticFuntion = scanner.nextLine();
			System.out.println("Salary: ");
			Double salary = scanner.nextDouble();
			return new Employee(firstName, lastName, cnp, getDidacticFunction(didacticFuntion), salary);
		}catch (Exception e){
			e.printStackTrace();
			return getEmployeeFromInput();
		}


	}

	private static DidacticFunction getDidacticFunction(String didacticFunction) {
		if (didacticFunction.toUpperCase().equals("ASISTENT"))
		{
			return DidacticFunction.ASISTENT;
		}
		if (didacticFunction.toUpperCase().equals("LECTURER"))
		{
			return DidacticFunction.LECTURER;
		}
		if (didacticFunction.toUpperCase().equals("TEACHER"))
		{
			return DidacticFunction.TEACHER;
		}
		if (didacticFunction.toUpperCase().equals("CONFERENTIAR"))
		{
			return DidacticFunction.CONFERENTIAR;
		}
		return DidacticFunction.ASISTENT;
	}

}
