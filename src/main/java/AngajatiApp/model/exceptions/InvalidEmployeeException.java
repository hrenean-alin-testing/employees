package AngajatiApp.model.exceptions;


// mutat din validator intr-un nou pachet exceptii
public class InvalidEmployeeException extends RuntimeException{

	private static final long serialVersionUID = 1460943029226542682L;
	
	public InvalidEmployeeException(){}
	
	public InvalidEmployeeException(String message) {
		super(message);
	}

}
